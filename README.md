# Frontend technical test for ZARA

This project is made to demonstrate knowledge about ReactJs

## Installation

Use the package manager [npm](https://docs.npmjs.com/getting-started) to install dependencies.

```bash
npm install
```

Once all dependencies are installed you can run the following scripts

Start to run the project in development

```bash
npm start
```

Build for production

```bash
npm build
```

To run test

```bash
npm test
```

To find and fix problems in your JavaScript code

```bash
npm lint
```

## Authors

[Luis Manurl Yerena Sosa](https://www.linkedin.com/in/luis-manuel-yerena-sosa/)

## License

[MIT](https://choosealicense.com/licenses/mit/)
